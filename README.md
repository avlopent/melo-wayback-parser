# Melo archiver, wayback edition

## Installation

(expects npm >= 6 and node… idk 15 probably?)

```shell
npm install
npm link
```

## Usage

`melo-parser [options]`

Important: this tool is destructive, it will overwrite anything that's in its way. If you're concerned about this, make sure to set the `--destination` option to an empty or even nonexistant directory.

### Options:

| Option | Description | Type |
|:------ | :---------- | :--: |
| `--version` | Show version number | `boolean` |
| `-u`, `--url` | A wayback url (web.archive.org) to parse. | `string` |
| `-p`, `--path` | A path containing markup from web.archive.org to parse. | `string` |
| `-y`, `--year` | The year the page was archived to determine parser. If absent, script will find year in URL. Required for local path. | `number` |
| `-v`, `--verbose` | Verbose logging. Use more 'v's for chattier logs. | `count` |
| `-d`, `--destination` | Destination directory (parent of '_posts')<br />Default `./app` | `string` |
| `--help` | Show help | `boolean` |

### Examples:

```shell
# Archive from Wayback Machine URL
melo-parser --url https://web.archive.org/web/20121013152756/http://www.melodramatic.com/users/sara
```

```shell
# Use markup from archive.org previously saved to disk
melo-parser --path ./foo/bar/markup.html --year 2012                              
```

## Development

This archiving script is based off of a script written in 2014 for backing up melo posts to be used in a Jekyll static site. There may be some useful abstraction that could still be done if SSGs have changed a ton.

As there is no compile step at the moment, `melo-parser` works perfectly fine for development as long as it is `npm link`-ed. If not, `npm run archive -- [options]` will work just as well.

### TODO

- [ ] fix touch & bang count on 2007 & 2008 parsers

#### Breaking Changes

- [ ] Instead of using `--url` or `--path` options, accept a single arg and determine if it's a URL or local file path. (deprecate the options, support until next major version)
- [ ] Break down design changes by month or even date
