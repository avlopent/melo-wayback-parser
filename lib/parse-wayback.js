import util from "util";
import cheerio from "cheerio";
import { log } from "./init.js";

// TODO possibly -1 a bunch of log levels here? a couple of these infos should
// be logs

function findUserName(year, $) {
  switch (year) {
    case 2001:
    case 2002:
    case 2003:
    case 2004:
      return $('input[name="journal_user"]').val();
    case 2005:
    case 2007:
    case 2008:
      if ($("body").has('input[name="journal_username"]')) {
        return $('input[name="journal_username"]').val();
      }

      return $(".normtext").find("b").first().text();
    case 2011:
    // tested on 20110618213653
    case 2013:
    case 2014:
      return $("#block-melodev-0").find("h1").text().trim();
    default:
      return "";
  }
}

function findFolderName(year, $) {
  const $body = $("body");

  switch (year) {
    case 2005:
    case 2007:
    case 2008:
      if ($body.has('input[name="folder_name"]')) {
        return $('input[name="folder_name"]').val();
      }

      // I'm not convinced this is necessary
      return $(".normtext")
        .find("b")
        .get()
        .map((element) => $(element).text())[1]
        .trim();
    case 2011:
    // tested on 20110618213653
    case 2013:
    case 2014:
      if ($body.hasClass("node-type-folder")) {
        return $("#content-header").find(".title").text().trim();
      }
    // falls through
    default:
      return "public";
  }
}

function findTitles(year, $, entries) {
  const captureTitle = (entry = {}, title = "") => {
    log.debug(`Found entry titled ${title}`);

    entry.title = title;

    return entry;
  };

  switch (year) {
    case 2001:
      const titleRegExp = /<b>\d{2}\.\d{2}\.\d{4}@\d{0,4}\s&nbsp;(.*)<\/b><br><br>/;

      $("div.normText").each((index, element) => {
        const [, title] = titleRegExp.exec($(element).html());

        entries[index] = captureTitle(entries[index], title);
      });

      return entries;
    case 2002:
    case 2003:
    case 2004:
      $(".journaltitle").each((index, element) => {
        entries[index] = captureTitle(entries[index], $(element).text().trim());
      });

      return entries;
    case 2005:
    case 2007:
    case 2008:
      $("h2.parsing-title").each((index, element) => {
        entries[index] = captureTitle(entries[index], $(element).text().trim());
      });

      return entries;
    case 2011:
    // tested on 20110618213653
    case 2013:
    case 2014:
      $(".node-type-blog").each((index, element) => {
        const id = $(element).attr("id").replace("node-", "");

        ["touch", "bang"].forEach((action) => {
          entries[index][action] = parseInt(
            $(element).find(`#${action}-count-${id}`).text(),
            10
          );
        });

        entries[index] = captureTitle(
          entries[index],
          $(element).find("h2.title").text().trim()
        );
        entries[index].id = id;
      });

      return entries;
    default:
      log.error(
        `No title parser available for year "${year}", specify one manually from available parsers`
      );

      return process.exit(1);
  }
}

function findDates(year, $, entries) {
  let $entries = $("div.parsing-entry");
  let dateTimeRegExp;
  const $body = $("body");
  const convertDuodecimal = (hours, duodecimal) => {
    const hour = duodecimal === "pm" ? `${parseInt(hours, 10) + 12}` : hours;

    return hour === "24" ? "00" : hour;
  };
  const captureDateString = (entry = {}, dateTime) => {
    // ISO8601 YYYY-MM-DDTHH:mm:ss.sssZ
    // String date DD MMM YYYY HH:mm:ss.sss Z
    log.debug(`Found a time for ${entry.title}`);
    log.trace(dateTime);

    entry.date = new Date(dateTime);

    log.trace(entry.date);

    return entry;
  };

  const dateTimeFormats = [
    {
      regExp: /(\d{2})\.(\d{2})\.(\d{4})@(\d{0,4})(\/(\d{2}):(\d{2}:\d{2})([ap]m))?/,
      formatter(matches) {
        const [, MM, DD, YYYY, beats] = matches;

        let dateString = `${YYYY}-${MM}-${DD}`;
        let timeString;

        if (matches.length > 7) {
          if (matches.length < 10) {
            timeString = `T${matches[6]}:${matches[7]}-08:00`;
          } else {
            timeString = `T${convertDuodecimal(matches[6], matches[8])}:${
              matches[7]
            }-08:00`;
          }
        } else {
          timeString = `T${convertedBeats(dateString, beats)}Z`;
        }

        return `${dateString}${timeString}`;
      },
    },
    {
      regExp: /([A-Z][a-z]{2})\.(\d{2})\.(20\d{2}) (\d{1,2}):(\d{2}:?\d{0,2})([ap]m)/,
      formatter: ([, MMM, DD, YYYY, HH, mmSS, duodecimal]) =>
        `${DD} ${MMM} ${YYYY} ${convertDuodecimal(
          HH,
          duodecimal
        )}:${mmSS} -08:00`,
    },
    {
      regExp: /(\d{4}-\d{2}-\d{2}) (\d{2}:\d{2}:\d{2})/,
      formatter: ([, date, time]) => `${date}T${time}-08:00`,
    },
    {
      regExp: /(\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}\.\d{3})\d*-\d{2}/,
      formatter: ([, dateTime]) => `${dateTime}-08:00`,
    },
    {
      regExp: /([A-Z][a-z]{2})\.(\d{2})\.(20\d{2}) (\d{1,2}):(\d{2})([ap]m)/,
      formatter(matches) {},
    },
  ];

  const convertedBeats = (dateString, beats) => {
    const date = new Date(dateString);
    const [seconds, ms] = (beats * 86.4).toString().split(".");

    date.setUTCSeconds(seconds, (ms || "0").padEnd(3, "0"));

    return `${date
      .getUTCHours()
      .toString()
      .padStart(2, "0")}:${date
      .getUTCMinutes()
      .toString()
      .padStart(2, "0")}:${date
      .getUTCSeconds()
      .toString()
      .padStart(2, "0")}.${date.getUTCMilliseconds()}`;
  };
  const text = $entries.text();

  switch (year) {
    case 2002:
    case 2003:
    case 2004:
      $entries = $entries.has(".journaltitle");
    // falls through
    case 2001:
    case 2005:
    case 2007:
    case 2008:
      const { regExp, formatter } = dateTimeFormats.find(({ regExp }) =>
        regExp.test(text)
      );

      $entries.each((index, element) => {
        entries[index] = captureDateString(
          entries[index],
          formatter(regExp.exec($(element).text()))
        );
      });

      return entries;
    case 2011:
      // tested on 20110618213653
      dateTimeRegExp = /[A-Z][a-z]{2}\.(\d{2})\.(\d{2})\.(\d{2}) (\d{1,2}):(\d{2})([ap]m)/;
    // falls through
    case 2013:
    case 2014:
      dateTimeRegExp =
        dateTimeRegExp ||
        /[A-Z][a-z]{2}, (\d{2})\/(\d{2})\/(\d{4}) - (\d{2}:\d{2})/;

      $(".node-type-blog").each((index, element) => {
        let dateTimeString;
        const metaText = $(element).find(".submitted").text();

        if (year === 2011) {
          const [
            ,
            month,
            day,
            year,
            hours,
            minutes,
            duodecimal,
          ] = metaText.match(dateTimeRegExp);

          dateTimeString = `20${year}-${month}-${day} ${convertDuodecimal(
            hours,
            duodecimal
          )}:${minutes} -08:00`;
        } else {
          const [, month, day, year, time] = metaText.match(dateTimeRegExp);

          dateTimeString = `${year}-${month}-${day}T${time}-08:00`;
        }

        entries[index] = captureDateString(entries[index], dateTimeString);
      });

      return entries;
    default:
      log.error(
        `No date parser available for year "${year}", specify one manually from available parsers`
      );

      return process.exit(1);
  }
}

function findBodyContent(year, $, entries) {
  let $entries = $("div.parsing-entry");
  const captureBodyMarkup = (entry = {}, body = "") => {
    log.debug(`Found a body for ${entry.title}`);

    entry.body = body.replace(/^ +/g, "");

    return entry;
  };

  switch (year) {
    case 2001:
    case 2002:
    case 2003:
    case 2004:
      if (year > 2001) {
        $entries = $entries.has(".journaltitle");
      }

      $entries.each((index, element) => {
        const $touch = $(element).find(".parsing-touch");

        entries[index].touch = parseInt($touch.text(), 10) || 0;
        $touch.remove();

        let markup = $(element).html();

        markup = markup
          // [ touch | ? ] link
          .replace(
            /<b>\[ <\/b><a href=".+">touch<\/a> <b>\/<\/b> <a href=".+">\?<\/a> <b>]<\/b><br>/,
            ""
          )
          .replace(/<\/span><br><br>/, "</span>")
          .replace(/<\/b>\s&nbsp;(\n)?/, "</b>$1")
          .replace(/&nbsp;<br>/, "")
          .replace(/(\n\s*){4,}/, "");

        $ = cheerio.load(markup);

        $("b").first().remove();
        $(".journaltitle").remove();

        entries[index] = captureBodyMarkup(entries[index], $("body").html());
      });

      return entries;
    case 2005:
      $entries.each((index, element) => {
        ["touch", "bang"].forEach((action) => {
          entries[index][action] = parseInt(
            $(element).find(`p.parsing-${action}`).text(),
            10
          );

          $(element).find(`p.parsing-${action}`).remove();
        });

        entries[index] = captureBodyMarkup(
          entries[index],
          $(element).find("div.parsing-content").html()
        );
      });

      return entries;
    case 2007:
      $entries.each((index, element) => {
        const $entry = $(element);

        ["touch", "bang"].forEach((action) => {
          entries[index][action] =
            parseInt(
              $entry.find(`.parsing-${action}-overflow`).text() || "0",
              10
            ) + $entry.find(`.parsing-${action}`).find(`img.${action}`).length;
        });

        captureBodyMarkup(entries[index], $entry.find(".JournalBody").html());
      });

      return entries;
    case 2008:
      $entries.each((index, element) => {
        const $entry = $(element);

        ["touch", "bang"].forEach((action) => {
          entries[index][action] =
            parseInt($entry.find(`[id^="${action}count_"]`).text(), 10) +
            $entry.find(`img.${action}`).length;
        });

        captureBodyMarkup(entries[index], $entry.find(".JournalBody").html());
      });

      return entries;

    case 2011:
    case 2013:
    case 2014:
      $(".node-type-blog").each((index, element) => {
        entries[index] = entries[index] || {};

        entries[index] = captureBodyMarkup(
          entries[index],
          $(element).find(".content").html()
        );
      });

      return entries;
    default:
      log.error(
        `No body parser available for year "${year}", specify one manually from available parsers`
      );

      return process.exit(1);
  }
}

export default function parseWayback(body, year) {
  if (body.toString().includes("This user has no public journal entries.")) {
    log.log("This page contains no entries.");

    process.exit(0);
  }

  let $;
  let entries = [];

  switch (year) {
    case 2001:
    case 2002:
    case 2003:
    case 2004:
      // TODO refactor like i did 05/07 that was brill
      $ = cheerio.load(
        body
          .toString()
          // Matches touches 20020603, 20020823
          .replace(
            /Touched: <b>(\d+)<\/b>/,
            "<span class='parsing-touch'>$1</span>"
          )
          .replace(
            /<a name="(\d{3,})"><\/a>(\n *)?<span class="normText">/g,
            '<a id="$1"></a><div class="parsing-entry">'
          )
          .replace(
            /<\/font>(\n *)?(<br>(\n *)?)+<hr>(\n *)?<br>(\n *)?<\/span>/g,
            "</div>"
          )
      );

      // matches touches in 20040202. capitalization is important here—it's the only place normtext isn't camelcased
      $(".parsing-entry > span.normtext").each((i, element) => {
        $(element)
          .addClass("parsing-touch")
          .removeClass("normtext")
          .text((index, text) => /\d+/.exec(text)[0]);
      });
      break;
    case 2005:
      $ = cheerio.load(
        body
          .toString()
          .replace(
            /<b>(.*)<\/b><br><br>([0-9:\- .]+)<br>/g,
            '<div class="parsing-entry"><h2 class="parsing-title">$1</h2>$2<div class="parsing-content">'
          )
          .replace(
            /Touched: (\d+) &nbsp; Banged: (\d+)<br><br>/g,
            '<p class="parsing-touch">$1</p><p class="parsing-bang">$2</p>'
          )
          .replace(/<br><br><hr>/g, "</div></div>")
      );
      break;
    case 2007:
      $ = cheerio.load(
        body
          .toString()
          .replace(/<td>\n?(\s*)<h1>/, `<td>\n$1<hr>\n$1<h1>`)
          .replace(
            /<hr>\n?\s*<h1>(.*?)<\/h1>/g,
            '<hr><div class="parsing-entry"><h2 class="parsing-title">$1</h2>'
          )
          .replace(
            /Touched:\n?((\s*<img src=".+" class="touch"\/>\n?)+)(\s*<b>[\s.]*\(\+(\d+)\)<\/b>)?/g,
            '<div class="parsing-touch">$1\n<span class="parsing-touch-overflow">$4</span></div>'
          )
          .replace(
            /Banged:\n?((\s*<img src=".+" class="bang"\/>\n?)+)(\s*<b>[\s.]*\(\+(\d+)\)<\/b>)?/g,
            '<div class="parsing-bang">$1\n<span class="parsing-bang-overflow">$4</span></div>'
          )
          .replace(/<\/div>\n?\s*<hr>\n?\s*<div/g, "</div></div><hr><div")
      );

      break;
    case 2008:
      $ = cheerio.load(
        body
          .toString()
          .replace(/<td>\n?(\s*)<h1>/, `<td>\n$1<hr>\n$1<h1>`)
          .replace(
            /<hr>\n?\s*<h1>(.*?)<\/h1>/g,
            '<hr><div class="parsing-entry"><h2 class="parsing-title">$1</h2>'
          )
          .replace(
            /Touched:\n((\s*<img src=".+" class="touch"\/>\n?)+)\s*<span id="touchview_(\d{3,})" style=".*">\n\s*<b>\n\s*\.\.\.\(\+\n\s*<span id="touchcount_\d{3,}">(\d+)<\/span>\n\s*\)\n\s*<\/b>\n\s*<\/span>\n\s*<br\/>\n/g,
            '<div class="parsing-touch" id="$2">$1\n<span class="parsing-touch-overflow">$3</span></div>'
          )
          .replace(
            /Banged:\n(\s*<img src=".+" class="bang"\/>\n)*\s*<span id="bangview_(\d{3,})" style=".*>\n\s*<b>\n\s*\.\.\.\(\+\n\s*<span id="bangcount_\d{3,}">(\d+)<\/span>\n\s*\)\n\s*<\/b>\n\s*<\/span>\n\s*<br\/>\n/g,
            '<div class="parsing-bang" id="$2">$1\n<span class="parsing-bang-overflow">$3</span></div>'
          )
          .replace(/<\/div>\n?\s*<hr>\n?\s*<div/g, "</div></div><hr><div")
      );

      break;
    case 2009:
      $ = cheerio.load(body);
      // TODO figure out the actual date of end of table.entries
      $ = cheerio.load(
        $("table.entries").find("tr").first().find("td").first().html()
      );
      break;
    default:
      $ = cheerio.load(body);
  }

  const userName = findUserName(year, $);
  const folderName = findFolderName(year, $);

  log.log(`Examining ${year} page for ${userName}, folder ${folderName}`);
  log.trace($("body").html().toString());

  log.info("Parsing entries");

  // TODO refactor… _gestures vaguely_ this
  entries = findTitles(year, $, entries);

  entries = findDates(year, $, entries);

  entries = findBodyContent(year, $, entries);

  entries.forEach((entry) => {
    entry.folder = folderName;

    log.debug("Complete entry compiled");
    log.debug(util.inspect(entry, false, 10, true));
  });

  return entries;
}
