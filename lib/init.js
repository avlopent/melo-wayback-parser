import { existsSync } from "fs";
import consola from "consola";
import yargs from "yargs";
import { hideBin } from "yargs/helpers";

const errors = [];

export const cliArgs = yargs(hideBin(process.argv))
  .scriptName("melo-parser")
  .usage("$0 [options]")
  .example([
    [
      "$0 --url https://web.archive.org/web/20121013152756/http://www.melodramatic.com/users/sara",
      "Archive from Wayback Machine URL",
    ],
    [
      "$0 --path ./foo/bar/markup.html --year 2012",
      "Use markup from archive.org previously saved to disk",
    ],
  ])
  .options({
    url: {
      alias: "u",
      conflicts: "path",
      description: "A wayback url (web.archive.org) to parse.",
      nargs: 1,
      type: "string",
    },
    path: {
      alias: "p",
      conflicts: "url",
      description: "A path containing markup from web.archive.org to parse.",
      implies: "year",
      nargs: 1,
      normalize: true,
      type: "string",
    },
    year: {
      alias: "y",
      demandOption: false,
      description:
        "The year the page was archived to determine parser. If absent, script will find year in URL. Required for local path.",
      nargs: 1,
      type: "number",
    },
    verbose: {
      alias: "v",
      count: true,
      description: "Verbose logging. Use more 'v's for chattier logs.",
      type: "boolean",
      nargs: 0,
    },
    destination: {
      alias: "d",
      default: "app",
      defaultDescription: "'app' (Jekyll `init`'s default location)",
      description: "Destination directory (parent of '_posts')",
      nargs: 1,
      type: "string",
    },
  })
  .check(({ url, path, year }) => {
    if (url && !/^https:\/\/web.archive.org\/web\/\d{14}/.test(url)) {
      errors.push("URL does not match expected Wayback Machine pattern.");
    }

    if (path && !existsSync(path)) {
      errors.push("File does not exist at provided path.");
    }

    if (year && (year < 2000 || year > 2020)) {
      errors.push(
        `Year "${year
          .toString()
          .substring(0, 3)}" does not match possible range of 2000-2020.`
      );
    }

    return errors.length <= 0;
  })
  .help().argv;

export const log = consola.create({ level: 2 + cliArgs.verbose });

if (errors.length > 0) {
  errors.forEach((errorString) => {
    log.error(errorString);
  });

  process.exit(1);
}
