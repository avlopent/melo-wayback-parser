import { mkdirSync, promises as fs } from "fs";
import path from "path";
import slugify from "@sindresorhus/slugify";
import { log } from "./init.js";

function formatFilename(date, title) {
  return `${date.getFullYear()}-${(date.getMonth() + 1)
    .toString()
    .padStart(2, "0")}-${date.getDate().toString().padStart(2, "0")}-${slugify(
    title
  )}.md`;
}

function safelySaveToDirectories(destination, folder) {
  if (folder === "public" || folder === "archive") {
    const folderPath = path.resolve(destination, "_posts");

    mkdirSync(folderPath, { recursive: true });

    return folderPath;
  } else if (folder) {
    const folderPath = path.resolve(destination, folder, "_posts");

    mkdirSync(folderPath, { recursive: true });

    return folderPath;
  } else {
    throw new Error("no folder found");
  }
}

export default function (entry, destination) {
  const { title, body, date, folder, touch, bang } = entry;
  const post = `---
layout: written-post
title: ${title}
date: ${date.toLocaleString("en-US", { timeZone: "America/Los_Angeles" })}
categories: ["${folder}"]
touches: ${touch > -1 ? touch : "0"}
bangs: ${bang > -1 ? bang : "0"}
---

${body}`;

  log.trace(formatFilename(date, title));

  // TODO introduce option to skip files that would overwrite
  return fs.writeFile(
    path.join(
      safelySaveToDirectories(destination, folder),
      formatFilename(date, title)
    ),
    post
  );
}
