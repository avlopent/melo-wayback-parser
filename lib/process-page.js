import parseWayback from "./parse-wayback.js";
import record from "./record.js";
import getMarkup from "./get-markup.js";

import { cliArgs, log } from "./init.js";

function parsedYear({ year, url }) {
  // TODO at a certain point, i'm going to need to specify designs more precisely than year
  if (year) {
    return year;
  }

  const yearMatch = url.match(
    /^https:\/\/web\.archive\.org\/web\/(20\d{2})\d{10}/
  );

  if (yearMatch[1]) {
    return parseInt(yearMatch[1], 10);
  }

  throw new Error(
    "Year is required as part of an archive.org url or as CLI arg."
  );
}

export default function processPage() {
  const { url, path, destination } = cliArgs;

  getMarkup(path ?? url, Boolean(path))
    .then((text) => parseWayback(text, parsedYear(cliArgs)))
    .catch((error) => {
      log.error(error);

      process.exit(1);
    })
    .then((entries) =>
      Promise.allSettled(entries.map((entry) => record(entry, destination)))
    )
    .then((postResults) => {
      let rejected = [];
      const success = [];

      log.info(`${postResults.length.toString()} entries processed.`);

      postResults.forEach((result) => {
        if (result.status === "rejected") {
          rejected.push(result.reason);
        } else {
          success.push(result.value);
        }
      });

      if (success.length > 0) {
        log.success(`Write succeeded on ${success.length.toString()} entries.`);
      }

      if (rejected.length > 0) {
        log.error(`Write failed on ${rejected.length.toString()} entries.`);

        rejected.forEach((reason) => {
          log.error(reason);
        });

        process.exit(1);
      }

      process.exit(0);
    });
}
