import { readFile } from "fs/promises";
import fetch from "node-fetch";

export default function (location, isLocal) {
  if (isLocal) {
    return readFile(location);
  }

  return fetch(location).then((body) => body.text());
}
